import os
import socket

import win32api
import win32con
from gooey import Gooey, GooeyParser
from xpinyin import Pinyin


def chineseToWord(name):
    py = Pinyin()
    pinyin = py.get_pinyin(name).split('-')
    result = ''.join([n.capitalize() for n in pinyin])
    return result
@Gooey(language='chinese', program_name="主机名修改工具", default_sixze=(500, 500), menu=[{
    'name': '关于',
    'items': [{
        'type': 'AboutDialog',
        'menuTitle': '关于',
        'name': '主机名修改工具',
        'description': '',
        'version': '1.0.0',
        'copyright': '2021',
        'website': 'https://zerlaer.com',
        'developer': 'Zerlaer',
        'license': 'MIT'
    }]
}, {
    'name': '作者',
    'items': [{
        'type': 'Link',
        'menuTitle': '小明同学',
        'url': 'https://zerlaer.com'
    }]
}])
def interface():
    msg = '输入自己的姓名后自动修改电脑主机名为"PC-Name"格式'
    parser = GooeyParser(description=msg)
    parser.add_argument("username", metavar='员工姓名', help="请输入自己的姓名,例如：张三")
    args = parser.parse_args()
    name = "PC-" + chineseToWord(args.username)
    os.system(
        f'reg add "HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\ComputerName\ActiveComputerName" /v ComputerName /t reg_sz /d {name} /f >nul 2>nul')
    os.system(
        f'reg add "HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\Tcpip\Parameters" /v "NV Hostname" /t reg_sz /d {name} /f >nul 2>nul')
    os.system(
        f'reg add "HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\Tcpip\Parameters" /v Hostname /t reg_sz /d {name} /f >nul 2>nul')
    hostname = socket.gethostname()
    win32api.MessageBox(0, f"主机名修改成功,新的主机名为:{hostname}" , "提示", win32con.MB_OK)


if __name__ == '__main__':
    interface()

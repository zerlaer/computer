package main

import (
	"fmt"
	"github.com/StackExchange/wmi"
)

type os struct {
	Caption        string
	OSArchitecture string
}

// OperatingSystem 系统信息
func OperatingSystem() {
	var (
		os []os
	)
	err := wmi.Query("Select * from Win32_OperatingSystem", &os)
	if err != nil {
		return
	}
	fmt.Println(os)
	
}

type cpu struct {
	Name string
}

// Processor CPU信息
func Processor() {
	var cpu []cpu
	err := wmi.Query("Select * from Win32_Processor", &cpu)
	if err != nil {
		return
	}
	fmt.Println(cpu)
	
}

type board struct {
	Caption      string // 主板名称
	SerialNumber string // 主板序列号
}

// BaseBoard 主板信息
func BaseBoard() {
	var board []board
	err := wmi.Query("Select * from Win32_BaseBoard", &board)
	if err != nil {
		return
	}
	fmt.Println(board)
	
}

type memory struct {
	Capacity uint64
}

func PhysicalMemory() {
	var memory []memory
	err := wmi.Query("Select * from Win32_PhysicalMemory", &memory)
	if err != nil {
		return
	}
	fmt.Println(memory)
	
}

type Storage struct {
	Name       string
	FileSystem string
	Total      uint64
	Free       uint64
}

type storage struct {
	Name       string
	Size       uint64
	FreeSpace  uint64
	FileSystem string
}

func LogicalDisk() {
	var storage []storage
	var loaclStorages []Storage
	err := wmi.Query("Select * from Win32_LogicalDisk", &storage)
	if err != nil {
		return
	}
	
	for _, storage := range storage {
		info := Storage{
			Name:       storage.Name,
			FileSystem: storage.FileSystem,
			Total:      storage.Size,
			Free:       storage.FreeSpace,
		}
		localStorages = append(loaclStorages, info)
	}
	fmt.Printf("localStorages:=",localStorages)
}

type network struct {
	DNSHostName string
}

func NetworkAdapter() {
	var network []network
	err := wmi.Query("Select * from Win32_NetworkAdapterConfiguration", &network)
	if err == nil {
		return
	}
	fmt.Println(network)
	
}

//type Network struct {
//	Name       string
//	IP         string
//	MACAddress string
//}
//
//type nic struct {
//	Name       string
//	MacAddress string
//	Ipv4       []string
//}

//func NetworkAdapter() error {
//	intf, err := net.Interfaces()
//	if err != nil {
//		return err
//	}
//	var is = make([]nic, len(intf))
//	for i, v := range intf {
//		ips, err := v.Addrs()
//		if err != nil {
//
//			return err
//		}
//		//此处过滤loopback（本地回环）和isatap（isatap隧道）
//		if !strings.Contains(v.Name, "Loopback") && !strings.Contains(v.Name, "isatap") {
//			var network Network
//			is[i].Name = v.Name
//			is[i].MacAddress = v.HardwareAddr.String()
//			for _, ip := range ips {
//				if strings.Contains(ip.String(), ".") {
//					is[i].Ipv4 = append(is[i].Ipv4, ip.String())
//				}
//			}
//			network.Name = is[i].Name
//			network.MACAddress = is[i].MacAddress
//			if len(is[i].Ipv4) > 0 {
//				network.IP = is[i].Ipv4[0]
//			}
//
//			fmt.Println(network)
//		}
//
//	}
//
//	return nil
//}
func main() {
	OperatingSystem()
	Processor()
	BaseBoard()
	PhysicalMemory()
	NetworkAdapter()
	
}
